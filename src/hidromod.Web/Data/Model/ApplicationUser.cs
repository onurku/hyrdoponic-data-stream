﻿using Microsoft.AspNetCore.Identity;

namespace hidromod.Web.Data.Model
{
    public class ApplicationUser : IdentityUser
    {
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Mobil { get; set; }
        
        public string AdSoyad
        {
            get { return Ad + " " + Soyad; }
        }
    }
}