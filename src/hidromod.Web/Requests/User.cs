using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using hidromod.Web.Data;
using hidromod.Web.Requests.Model;

namespace hidromod.Web.Requests
{
    public class User
    {
        public class GetEmailConfirmedUserList : IRequest<ResultDto<UserDto[]>>
        {
            
        }

        public class Handler :  IRequestHandler<GetEmailConfirmedUserList, ResultDto<UserDto[]>>
        {
            private readonly hidromodDbContext _context;
            private readonly IMapper _mapper;

            public Handler(hidromodDbContext context, IMapper mapper)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }
            
            public async Task<ResultDto<UserDto[]>> Handle(GetEmailConfirmedUserList request, CancellationToken cancellationToken)
            {               
                var result = new ResultDto<UserDto[]>();
                
                var list = _context.Users.Where(x => x.EmailConfirmed).AsQueryable();
                
                result.Result = await list.Select(x=> _mapper.Map<UserDto>(x)).ToArrayAsync(cancellationToken);

                return result;
            }
        }
    }
}