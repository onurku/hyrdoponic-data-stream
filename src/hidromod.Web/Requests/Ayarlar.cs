using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using hidromod.Web.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using hidromod.Web.Requests.Model;

namespace hidromod.Web.Requests
{
    public static class Ayarlar
    {
        public class GetAyarlar : IRequest<ResultDto<AyarlarDto[]>>
        {
        }

          public class GetAyarlarByGrup : IRequest<ResultDto<AyarlarDto[]>>
        {
            public int GrupId { get; set; }
        }
        
        public class GetByIdAyarlar : IRequest<ResultDto<AyarlarDto>>
        {
            public int Id { get; set; }
        }
        
        public class SaveAyarlar : IRequest<ResultDto<AyarlarDto>>
        {
            public int Id { get; set; }
            public AyarlarPostDto Post { get; set; }
        }
        
        public class DeleteAyarlar : IRequest<ResultDto<AyarlarDto>>
        {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<GetAyarlar, ResultDto<AyarlarDto[]>>, IRequestHandler<GetByIdAyarlar, ResultDto<AyarlarDto>>, 
                    IRequestHandler<SaveAyarlar, ResultDto<AyarlarDto>>, IRequestHandler<DeleteAyarlar, ResultDto<AyarlarDto>>
        {
            private readonly hidromodDbContext _context;
            private readonly IMapper _mapper;

            public Handler(hidromodDbContext context, IMapper mapper)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }
            
            public async Task<ResultDto<AyarlarDto[]>> Handle(GetAyarlar request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<AyarlarDto[]>();
                
                var list = _context.Ayarlar.AsQueryable();
                
                result.Result = await list.Select(x=> _mapper.Map<AyarlarDto>(x)).ToArrayAsync();

                return result;
            }

            public async Task<ResultDto<AyarlarDto>> Handle(SaveAyarlar request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<AyarlarDto>();
                if (request.Post == null) throw new ArgumentNullException(nameof(request.Post));

                var entity = _mapper.Map<Data.Model.Ayarlar>(request.Post);

                if (request.Id <= default(int))
                {
                    _context.Ayarlar.Add(entity);
                }
                else
                {
                    entity.Id = request.Id;
                    _context.Ayarlar.Update(entity);
                }

                await _context.SaveChangesAsync(cancellationToken);
                result.Result = _mapper.Map<AyarlarDto>(entity);

                return result;
            }

            public  async Task<ResultDto<AyarlarDto>> Handle(DeleteAyarlar request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<AyarlarDto>();
                if (request.Id <= default(int)) throw new ArgumentNullException(nameof(request.Id));

                var entity = new Data.Model.Ayarlar {Id = request.Id};
                _context.Ayarlar.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);
                result.Result = _mapper.Map<AyarlarDto>(entity);

                return result;
            }

            public async Task<ResultDto<AyarlarDto>> Handle(GetByIdAyarlar request, CancellationToken cancellationToken)
            {
                var result = new ResultDto<AyarlarDto>();
                if(request.Id <= default(int)) throw new ArgumentNullException(nameof(request.Id));
                
                var entity = await _context.Ayarlar.Include("Grup").FirstOrDefaultAsync(x=> x.Id == request.Id, cancellationToken: cancellationToken);

                if (entity != null)
                {
                    result.Result = _mapper.Map<AyarlarDto>(entity);   
                }
                else
                {
                    result.Error = ResultError.NotFound;
                }
                
                return result;
            }
        }
    }
}