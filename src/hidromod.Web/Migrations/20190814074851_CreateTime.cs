﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace hidromod.Web.Migrations
{
    public partial class CreateTime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "hidromod.Audit",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Ph = table.Column<decimal>(nullable: false),
                    Tds = table.Column<decimal>(nullable: false),
                    CreateTime = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_hidromod.Audit", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "hidromod.Ayarlar",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    closingTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    closingInterval = table.Column<decimal>(nullable: false),
                    nutrientDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    phLevel = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_hidromod.Ayarlar", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "hidromod.Audit");

            migrationBuilder.DropTable(
                name: "hidromod.Ayarlar");
        }
    }
}
