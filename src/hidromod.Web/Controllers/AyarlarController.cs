using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using hidromod.Web.Requests;
using hidromod.Web.Requests.Model;

namespace hidromod.Web.Controllers
{
    [Route("api/[controller]")]
    public class AyarlarController : Controller
    {
        private readonly IMediator _mediator;

        public AyarlarController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Sistemdeki tüm Ayarlarler
        /// </summary>
        /// <returns>IList</returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResultDto<AyarlarDto[]>), 200)]
        public async Task<ResultDto<AyarlarDto[]>> GetAll()
        {
            try
            {
                return await _mediator.Send(new Ayarlar.GetAyarlar());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

                /// <summary>
        /// Sistemdeki tüm Urunler
        /// </summary>
        /// <param name="id">Grup Id</param>
        /// <param name="kategoriIds">Kategori Id list to filter</param>
        /// <returns>IList</returns>
        [HttpGet("grup/{id}")]
        [ProducesResponseType(typeof(ResultDto<AyarlarDto[]>), 200)]
        public async Task<ResultDto<AyarlarDto[]>> GetByGrup(int id)
        {
            try
            {
                return await _mediator.Send(new Requests.Ayarlar.GetAyarlarByGrup{ GrupId = id});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        /// <summary>
        /// Id ile Ayarlar
        /// </summary>
        /// <returns>Ayarlar</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ResultDto<AyarlarDto>), 200)]
        [ProducesResponseType(typeof(void), 404)]
        public async Task<ResultDto<AyarlarDto>> GetById(int id)
        {
            try
            {
                return await _mediator.Send(new Ayarlar.GetByIdAyarlar{ Id = id});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        /// <summary>
        /// Ayarlar ekle
        /// </summary>
        /// <returns>Ayarlar</returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResultDto<AyarlarDto>), 200)]
        public async Task<ResultDto<AyarlarDto>> Create([FromBody]AyarlarPostDto post)
        {
            try
            {
                return await _mediator.Send(new Ayarlar.SaveAyarlar{ Post = post});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        /// <summary>
        /// Ayarlar guncelle
        /// </summary>
        /// <returns>Ayarlar</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ResultDto<AyarlarDto>), 200)]
        public async Task<ResultDto<AyarlarDto>> Update(int id, [FromBody]AyarlarPostDto post)
        {
            try
            {
                return await _mediator.Send(new Ayarlar.SaveAyarlar{ Id=id, Post = post});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        /// <summary>
        /// Ayarlar sil
        /// </summary>
        /// <returns>Ayarlar</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(ResultDto<AyarlarDto>), 200)]
        public async Task<ResultDto<AyarlarDto>> Delete(int id)
        {
            try
            {
                return await _mediator.Send(new Ayarlar.DeleteAyarlar{ Id = id });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}