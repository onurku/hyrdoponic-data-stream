export const environment = {
  production: true,
  baseUrl: 'http://www.api.canhydroponic.com', // "http://betaapi.ekotopluluk.org", //Change this to the address of your backend API if different from frontend address
  loginUrl: "/login"
};
