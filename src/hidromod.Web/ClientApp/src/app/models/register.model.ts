export class Register {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(email?: string, currentPassword?: string, newPassword?: string,
        isLockedOut?: boolean, id?: string, userName?:
      string, ad?: string, soyad?: string, mobil?: string,jobTitle?: string,
      phoneNumber?: string, configuration?: string,  roles?: string[]) {

      this.currentPassword = currentPassword;
      this.newPassword = newPassword;
      this.isLockedOut = isLockedOut;
      this.id = id;
      this.userName = userName;
      this.ad = ad;
      this.soyad = soyad;
      this.mobil = mobil;
      this.email = email;
      this.jobTitle = jobTitle;
      this.phoneNumber = phoneNumber;
      this.configuration = configuration;
      this.roles = roles;
    }
    public ad: string;
    public soyad: string;
    public mobil: string;
    public currentPassword: string;
    public newPassword: string;
    public isLockedOut: boolean;
    public id: string;
    public userName: string;
    public fullName: string;
    public email: string;
    public jobTitle: string;
    public phoneNumber: string;
    public configuration: string;
    public roles: string[];
  }
