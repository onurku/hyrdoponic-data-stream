export class Audit {
    id: number;
    ph: number;
    tds: number;
    createTime: Date;
  }
