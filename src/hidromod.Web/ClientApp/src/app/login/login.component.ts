import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DataSharingService } from '../services/data-sharing.service';
import { Router } from '@angular/router';
import {
  AuthService,
  AlertService,
  ConfigurationService,
  DialogType,
  MessageSeverity,
  Utilities
} from '../services';

import { UserLogin } from '../models';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [DataSharingService]
})

export class LoginComponent implements OnInit, OnDestroy {
  userLogin = new UserLogin();
  isLoading = false;
  formResetToggle = true;
  modalClosedCallback: () => void;
  loginStatusSubscription: any;

  @Input()
  isModal = false;

  constructor(private alertService: AlertService, private authService: AuthService,
    private configurations: ConfigurationService, private ngxService: NgxUiLoaderService,
    private sharingService: DataSharingService, private router: Router) {

  }

  ngOnInit() {
    this.userLogin.rememberMe = this.authService.rememberMe;

    if (this.getShouldRedirect()) {
      this.authService.redirectLoginUser();
    } else {
      this.loginStatusSubscription = this.authService.getLoginStatusEvent().subscribe(isLoggedIn => {
        if (this.getShouldRedirect()) {
          this.authService.redirectLoginUser();
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.loginStatusSubscription) {
      this.loginStatusSubscription.unsubscribe();
    }
  }


  getShouldRedirect() {
    return !this.isModal && this.authService.isLoggedIn && !this.authService.isSessionExpired;
  }


  showErrorAlert(caption: string, message: string) {
    this.alertService.showStickyMessage(caption, message, MessageSeverity.error);
  }

  closeModal() {
    if (this.modalClosedCallback) {
      this.modalClosedCallback();
    }
  }


  login() {

    this.ngxService.startLoader('loading1'); // start foreground spinner of the loader 'loader-01' with 'default' taskId

    this.authService.login(this.userLogin.email, this.userLogin.password, this.userLogin.rememberMe)
      .subscribe(
        user => {
          console.log('user ID is :' + user.id);
          setTimeout(() => {
            this.ngxService.stopLoader('loading1');
            this.reset();
            if (!this.isModal) {
              this.alertService.showStickyMessage('Hoşgeldin', `Hoşgeldin ${user.userName}!`, MessageSeverity.success, false);
            } else {
              this.alertService.showStickyMessage('Login', `Oturum ${user.userName} restored!`, MessageSeverity.success, false);
              setTimeout(() => {
                this.alertService.showStickyMessage('Denedik olmadı...', 'Bi tekrar dener misin ? ', MessageSeverity.default, false);
              }, 500);
              this.closeModal();
            }
          }, 500);
        },
        error => {

          this.alertService.stopLoadingMessage();

          if (Utilities.checkNoNetwork(error)) {
            this.alertService.showToast(Utilities.noNetworkMessageCaption,
              Utilities.noNetworkMessageDetail, MessageSeverity.error, true);

            this.offerAlternateHost();
          } else {
            const errorMessage = Utilities.findHttpResponseMessage('error_description', error);

            if (errorMessage) {
              this.alertService.showToast('<h2>Giriş Sağlanamadı</h2>', errorMessage, MessageSeverity.error, false);
            } else {
              this.alertService.showToast('<h2>Bağlantı Hatası</h2>', 'Bir bağlantı hatası oluştu. Lütfen sonra tekrar deneyiniz. ',
                MessageSeverity.error, false);
            }
          }

          setTimeout(() => {
            this.ngxService.stopLoader('loading1');
          }, 500);
        });
  }


  offerAlternateHost() {

    if (Utilities.checkIsLocalHost(location.origin) && Utilities.checkIsLocalHost(this.configurations.baseUrl)) {
      this.alertService.showDialog('Dear Developer!\nIt appears your backend Web API service is not running...\n' +
        'Would you want to temporarily switch to the online Demo API below?(Or specify another)',
        DialogType.prompt,
        (value: string) => {
          this.configurations.baseUrl = value;
          this.alertService.showStickyMessage('API Changed!', 'The target Web API has been changed to: ' + value, MessageSeverity.warn);
        },
        null,
        null,
        null,
        this.configurations.fallbackBaseUrl);
    }
  }


  reset() {
    this.formResetToggle = false;

    setTimeout(() => {
      this.formResetToggle = true;
    });
  }

  toRegisterForm() {
    this.router.navigate(['/register']);
  }
}
