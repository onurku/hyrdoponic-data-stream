import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SettingsComponent } from './settings/settings.component';
import { AuthGuard } from './services/auth-guard.service';
import { RegisterComponent } from './register/register.component';

export const appRouting: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard], data: { title: 'Ayarlar' } },
  { path: 'register', component: RegisterComponent, data: { title: 'Kullanıcı Oluşturma' } }
];
