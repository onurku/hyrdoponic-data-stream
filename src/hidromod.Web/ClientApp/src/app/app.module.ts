import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxUiLoaderModule } from 'ngx-ui-loader';

import { AppErrorHandler } from './app-error.handler';

import { appRouting } from './app-routing.module';
import { HomeModule } from './home/home.module';
import { LoginModule } from './login/login.module';
import { NavMenuModule } from './nav-menu/nav-menu.module';
import { ServicesModule } from './services/services.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppComponent } from './app.component';
import { ModalComponent } from '../app/tools/modelView/modelView.component';
import { SettingsComponent } from './settings/settings.component';
import { UserInfoComponent } from './settings/user-info.component';
import { UserPreferencesComponent } from './settings/user-preferences.component';
import { UsersManagementComponent } from './settings/users-management.component';
import { ToastaModule } from 'ngx-toasta';
import { AlertService } from './services/alert.service';
import { RegisterModule } from './register/register.module';
import { library } from '@fortawesome/fontawesome-svg-core';

import { faGlasses
} from '@fortawesome/free-solid-svg-icons';
library.add( faGlasses );
@NgModule({
  declarations: [
    AppComponent,
    SettingsComponent,
    UserInfoComponent,
    UserPreferencesComponent,
    UsersManagementComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    FormsModule,
    HomeModule,
    HttpClientModule,
    LoginModule,
    NavMenuModule,
    NgbModule,
    NgxDatatableModule,
    RouterModule.forRoot(appRouting),
    ServicesModule,
    ToastaModule.forRoot(),
    NgxUiLoaderModule.forRoot({}),
    RegisterModule,
    FontAwesomeModule
  ],
  providers: [
    { provide: 'BASE_URL', useFactory: getBaseUrl },
    { provide: ErrorHandler, useClass: AppErrorHandler },
    AlertService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}
