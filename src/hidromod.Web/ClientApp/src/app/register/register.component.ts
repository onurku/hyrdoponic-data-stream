import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Register } from '../models/register.model';
import { trigger, transition, style, animate } from '@angular/animations';
import {
  AuthService,
  AlertService,
  MessageSeverity
} from '../services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({ opacity: 0 }),
        animate(500, style({ opacity: 1 }))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(500, style({ opacity: 0 }))
      ])
    ])],
})

export class RegisterComponent implements OnInit, OnDestroy {
  public userRegister = new Register();
  public AfterRegister: boolean;
  public EmailVar: boolean;
  public geciciSifre: string;
  public isLoading = false;
  formResetToggle = true;

  constructor(private alertService: AlertService, private authService: AuthService,
     private ngxService: NgxUiLoaderService) {
  }

  ngOnInit() {
    this.userRegister.configuration = '';
    this.userRegister.fullName = this.userRegister.email;
    this.userRegister.isLockedOut = false;
    this.userRegister.jobTitle = '';
    this.userRegister.phoneNumber = '';
    this.userRegister.userName =  this.userRegister.email;
    this.userRegister.roles = ['user'];
  }

  ngOnDestroy() {
  }

  showErrorAlert(caption: string, message: string) {
    this.alertService.showStickyMessage(caption, message, MessageSeverity.error);
  }

  passGenerate () {
    const chars = '0123456789abcdefghiklmnopqrstuvwxyz';
    const string_length = 8;
    let randomstring = '';
    for (let i = 0; i < string_length; i++) {
      const rnum = Math.floor(Math.random() * chars.length);
      randomstring += chars.substring(rnum , rnum + 1);
    }
    return randomstring;
  }

  register() {
    this.ngxService.startLoader('loading1');
    this.isLoading = true;
    const passSt = this.passGenerate();
    this.geciciSifre = passSt;
    this.userRegister.userName =  this.userRegister.email;
    this.userRegister.ad =  this.userRegister.ad;
    this.userRegister.soyad =  this.userRegister.soyad;
    this.userRegister.newPassword = passSt;
    this.userRegister.currentPassword = passSt;
    this.authService.register(this.userRegister).subscribe(res => {
          console.log(' RES : ' + JSON.stringify(res));
          if (res) {
            setTimeout(() => {
              this.AfterRegister = true;
            }, 1000);
          }
            // if ( res.toString() === '200' ) {
            //   this.alertService.showStickyMessage('Kullanıcı Oluşturuldu', `Şimdiden Hoşgeldin
            //         ${this.userRegister.email}!`, MessageSeverity.success, false);
            //         this.AfterRegister = true;
            // } else if (res.toString() === '300') {
            //   this.AfterRegister = true;
            //   this.EmailVar = true;
            // }
            this.ngxService.stopLoader('loading1');
            this.isLoading = false;
        }, error => {
          this.isLoading = false;
          this.alertService.showStickyMessage('Bir sorun oluştu:', 'Tekrar dener misin ?', MessageSeverity.error, false);
          setTimeout(() => {
            this.ngxService.stopLoader('loading1');
          }, 500);
        });
  }

  reset() {
    this.formResetToggle = false;

    setTimeout(() => {
      this.formResetToggle = true;
    });
  }
}
