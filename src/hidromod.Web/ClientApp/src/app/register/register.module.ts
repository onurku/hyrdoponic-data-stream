import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { RegisterComponent } from './register.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxUiLoaderModule.forRoot({})
  ],
  declarations: [
    RegisterComponent,
  ],
})
export class RegisterModule { }

