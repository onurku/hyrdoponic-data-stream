import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { HidromodEndpoint } from './hidromod-endpoint.service';
import { AuthService } from './auth.service';
import { ApiResult, Audit, Ayarlar } from '../models';

@Injectable()
export class HidromodService {

  private subject = new Subject<any>();

  constructor(private authService: AuthService, private hidromodEndpoint: HidromodEndpoint) { }

  getAllAuditLast100() {
    return this.hidromodEndpoint.getAllAuditLast100<ApiResult<Audit[]>>();
  }

  addAudit(ph: number, tds: number) {
    return this.hidromodEndpoint.addAuditEndpoint<ApiResult<Audit>>(ph, tds);
  }

  getAvarageAudit(st: string) {
    return this.hidromodEndpoint.getAvarageAudit<ApiResult<number>>(st);
  }

  sendMessage(message: string) {
    this.subject.next({ text: message });
  }

  clearMessage() {
    this.subject.next();
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }


  getcurrentUser() {
    return this.authService.currentUser;
  }


}
