import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HidromodService } from '../services/hidromod.service';
import { ApiResult } from '../models/apiresult.model';
import { AccountService } from './account.service';

@Injectable({
  providedIn: 'root'
})
export class DataSharingService {

  // tslint:disable-next-line:no-unused-expression
  private kategoriID: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private GrupId: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private AktifGrup: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  currentCategory = this.kategoriID.asObservable();
  aktifGrup = this.AktifGrup.asObservable();
  constructor(private HidromodService: HidromodService,
    private Account: AccountService) { }
  changeKategori(newVal: number) {
    this.kategoriID.next(newVal);
  }
  changeGrup(newVal: number) {
    this.GrupId.next(newVal);
  }
}
