// ====================================================
// More Templates: https://www.ebenmonney.com/templates
// Email: support@ebenmonney.com
// ====================================================

import { Injectable } from '@angular/core';
import { HttpResponseBase } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Utilities } from './utilities';
import { ToastaService, ToastOptions, ToastData } from 'ngx-toasta';

@Injectable()
export class AlertService {
  private messages = new Subject<AlertMessage>();
  private stickyMessages = new Subject<AlertMessage>();
  private dialogs = new Subject<AlertDialog>();
  private _isLoading = false;
  private loadingMessageId: any;
  private stickyToasties: number[] = [];

  constructor(public toastaService: ToastaService) { }

  showDialog(message: string)
  showDialog(message: string, type: DialogType, okCallback: (val?: any) => any)
  showDialog(message: string, type: DialogType, okCallback?: (val?: any) => any, cancelCallback?: () => any, okLabel?: string, cancelLabel?: string, defaultValue?: string)
  showDialog(message: string, type?: DialogType, okCallback?: (val?: any) => any, cancelCallback?: () => any, okLabel?: string, cancelLabel?: string, defaultValue?: string) {

    if (!type)
      type = DialogType.alert;

    this.dialogs.next({ message: message, type: type, okCallback: okCallback, cancelCallback: cancelCallback, okLabel: okLabel, cancelLabel: cancelLabel, defaultValue: defaultValue });
  }

  showMessage(summary: string)
  showMessage(summary: string, detail: string, severity: MessageSeverity)
  showMessage(summaryAndDetails: string[], summaryAndDetailsSeparator: string, severity: MessageSeverity)
  showMessage(response: HttpResponseBase, ignoreValue_useNull: string, severity: MessageSeverity)
  showMessage(data: any, separatorOrDetail?: string, severity?: MessageSeverity) {
   // console.log('asdasdasd');
    if (!severity)
      severity = MessageSeverity.default;

    if (data instanceof HttpResponseBase) {
      data = Utilities.getHttpResponseMessage(data);
      separatorOrDetail = Utilities.captionAndMessageSeparator;
    }

    if (data instanceof Array) {
      for (let message of data) {
        let msgObject = Utilities.splitInTwo(message, separatorOrDetail);

        this.showMessageHelper(msgObject.firstPart, msgObject.secondPart, severity, false);
      }
    } else {
      this.showMessageHelper(data, separatorOrDetail, severity, false);
    }
  }


  showStickyMessage(summary: string, detail: string, severity: MessageSeverity, error?: any) {
    // console.log(' Show Stcky Message Runs.');
    if (!severity) {
      severity = MessageSeverity.default;
    }
    this.showMessageHelper(summary, detail, severity, true);
  }

  private showMessageHelper(summary: string, detail: string, severity: MessageSeverity, isSticky: boolean) {
    if (isSticky) {
      this.stickyMessages.next({ severity: severity, summary: summary, detail: detail });
    } else {
      this.messages.next({ severity: severity, summary: summary, detail: detail });
    }
  }

  startLoadingMessage(message = 'Loading...', caption = '') {
    this._isLoading = true;
    clearTimeout(this.loadingMessageId);

    this.loadingMessageId = setTimeout(() => {
      this.showStickyMessage(caption, message, MessageSeverity.wait);
    }, 1000);
  }

  stopLoadingMessage() {
    this._isLoading = false;
    clearTimeout(this.loadingMessageId);
    this.resetStickyMessage();
  }

  logDebug(msg) {
    console.debug(msg);
  }

  logError(msg) {
    console.error(msg);
  }

  logInfo(msg) {
    console.info(msg);
  }

  logMessage(msg) {
    console.log(msg);
  }

  logTrace(msg) {
    console.trace(msg);
  }

  logWarning(msg) {
    console.warn(msg);
  }

  resetStickyMessage() {
    this.stickyMessages.next();
  }

  getDialogEvent(): Observable<AlertDialog> {
    return this.dialogs.asObservable();
  }

  getMessageEvent(): Observable<AlertMessage> {
    console.log('getMessageEvent RUNS');
    return this.messages.asObservable();
  }

  getStickyMessageEvent(): Observable<AlertMessage> {
    console.log('getMessageEvent RUNS2');
    return this.stickyMessages.asObservable();
  }

  get isLoadingInProgress(): boolean {
    return this._isLoading;
  }

  showToast(summary: string, detail: string, severity: MessageSeverity, isSticky: boolean) {
    if (detail == null) {
      return;
    }
    const toastOptions: ToastOptions = {
      title: summary,
      msg: detail,
      showClose: true,
      timeout: isSticky ? 0 : 5000
    };

    if (isSticky) {
      toastOptions.onAdd = (toast: ToastData) => this.stickyToasties.push(toast.id);
      toastOptions.onRemove = (toast: ToastData) => {
        const index = this.stickyToasties.indexOf(toast.id, 0);
        if (index > -1) {
          this.stickyToasties.splice(index, 1);
        }
        toast.onAdd = null;
        toast.onRemove = null;
      };
    }
    switch (severity) {
      case MessageSeverity.default: this.toastaService.default(toastOptions); break;
      case MessageSeverity.info: this.toastaService.info(toastOptions); break;
      case MessageSeverity.success: this.toastaService.success(toastOptions); break;
      case MessageSeverity.error: this.toastaService.error(toastOptions); break;
      case MessageSeverity.warn: this.toastaService.warning(toastOptions); break;
      case MessageSeverity.wait: this.toastaService.wait(toastOptions); break;
    }
  }
}


//******************** Dialog ********************//
export class AlertDialog {
  constructor(public message: string, public type: DialogType, public okCallback: (val?: any) => any, public cancelCallback: () => any,
    public defaultValue: string, public okLabel: string, public cancelLabel: string) {

  }
}

export enum DialogType {
  alert,
  confirm,
  prompt
}
//******************** End ********************//

//******************** Growls ********************//
export class AlertMessage {
  constructor(public severity: MessageSeverity, public summary: string, public detail: string) { }
}

export enum MessageSeverity {
  default,
  info,
  success,
  error,
  warn,
  wait
}
//******************** End ********************//
