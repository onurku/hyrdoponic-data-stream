import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountEndpoint } from './account-endpoint.service';
import { AccountService } from './account.service';
import { AlertService } from './alert.service';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth-guard.service';
import { ConfigurationService } from './configuration.service';
import { EndpointFactory } from './endpoint-factory.service';
import { LocalStoreManager } from './local-store-manager.service';

import { HidromodEndpoint } from './hidromod-endpoint.service';
import { HidromodService } from './hidromod.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  providers: [
    AccountEndpoint,
    AccountService,
    AlertService,
    AuthService,
    AuthGuard,
    ConfigurationService,
    EndpointFactory,
    LocalStoreManager,
    HidromodEndpoint,
    HidromodService,
  ],
})
export class ServicesModule { }
