import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';
import { NumberFormatStyle } from '@angular/common';

@Injectable()
export class HidromodEndpoint extends EndpointFactory {

  private readonly _auditUrl: string = '/api/Audit';


  get auditUrl() { return this.configurations.baseUrl + this._auditUrl; }


  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {
    super(http, configurations, injector);
  }


  getAllAuditLast100<T>(): Observable<T> {
    return this.http.get<T>(this.auditUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getAllAuditLast100());
      });
  }

  getAvarageAudit<T>(st: string): Observable<T> {
    const endpointUrl = `${this.auditUrl}/Avg/${st}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getAllAuditLast100());
      });
  }

  addAuditEndpoint<T>(ph: number, tds: number): Observable<T> {
    const endpointUrl = `${this.auditUrl}/ph/${JSON.stringify(ph)}/tds/${JSON.stringify(tds)}`;
    return this.http.post<T>(endpointUrl,  this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.addAuditEndpoint<T>(ph, tds));
      });
  }
}
