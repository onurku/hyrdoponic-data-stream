import { Component, OnInit } from '@angular/core';
import { AuthService, LocalStoreManager, AccountService } from '../services';
import { trigger, transition, animate, style } from '@angular/animations';
import { DataSharingService } from '../services/data-sharing.service';
@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({ opacity: 0 }),
        animate(500, style({ opacity: 1 }))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(500, style({ opacity: 0 }))
      ])
    ])]
})
export class NavMenuComponent implements OnInit {
  public kullaniciAdi = '';
  public isAdmin = false;
  isAppLoaded: boolean;
  private _isUserLoggedIn: boolean;
  public get isUserLoggedIn(): boolean {
    return this._isUserLoggedIn;
  }
  public set isUserLoggedIn(value: boolean) {
    this._isUserLoggedIn = value;
  }

  constructor(storageManager: LocalStoreManager,
    private authService: AuthService,
    private Account: AccountService,
    private sharingService: DataSharingService) {
    storageManager.initialiseStorageSyncListener();
  }

  ngOnInit() {

    this.isUserLoggedIn = this.authService.isLoggedIn;
    // 1 sec to ensure all the effort to get the css animation working is appreciated :|, Preboot screen is removed .5 sec later
    setTimeout(() => this.isAppLoaded = true, 1000);

    this.authService.getLoginStatusEvent().subscribe(isLoggedIn => {
      this.isUserLoggedIn = isLoggedIn;
      setTimeout(() => {
        if (isLoggedIn) {
          this.kullaniciAdi = isLoggedIn ?  this.Account.currentUser.userName : 'null';
          if (this.Account.currentUser.roles.includes('administrator')) {
            this.isAdmin = true;
          }
        }
      }, 500);
    });
  }

  logout() {
    this.authService.logout();
    this.kullaniciAdi = '';
    this.isAdmin = false;
    this.authService.redirectLogoutUser();
  }


}
