import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  faHeart
} from '@fortawesome/free-solid-svg-icons';
library.add(faHeart);
import { HomeComponent } from './home.component';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { OutputGraphComponent } from '../output-graph/output-graph.component';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  declarations: [
    HomeComponent,
    OutputGraphComponent,
  ],
})
export class HomeModule { }
