import { Component } from '@angular/core';
import { HidromodService } from '../services/hidromod.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  constructor(
    public hidromodService: HidromodService
  ) { }

  AuditGonder() {
    const ph = Math.floor(Math.random() * 12) + 1;
    const tds = Math.floor(Math.random() * 1000) + 1;
    console.log(' Gönder RUNS' + ph + '  ' + tds);
    this.hidromodService.addAudit(ph, tds).subscribe(val => {
      console.log(' response :' + JSON.stringify(val));
    });
  }
}
