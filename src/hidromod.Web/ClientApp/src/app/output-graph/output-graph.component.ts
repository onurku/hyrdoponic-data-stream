import { Component, OnInit, Inject } from '@angular/core';
import { Subscription } from 'rxjs/subscription';
import { HidromodService } from '../services/hidromod.service';
import * as Highcharts from 'highcharts';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const Solid = require('highcharts/modules/solid-gauge');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');


Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);
Solid(Highcharts);

@Component({
  selector: 'app-output-graph',
  templateUrl: './output-graph.component.html',
  styleUrls: ['./output-graph.component.css']
})
export class OutputGraphComponent implements OnInit {

  public phListoptions: any = {

    chart: {
      height: 700,
      backgroundColor: 'transparent'
    },
    title: {
      text: 'Ph Tds List'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      type: 'datetime',
      labels: {
        formatter: function() {
          return Highcharts.dateFormat('%H:%M:%S', this.value);
        }
      },
      plotLines: []
    },
    yAxis: [{
      title: {
        text: 'Ph Values',
        style: {
          color: Highcharts.getOptions().colors[0]
        }
      },
      labels: {
        format: '{value}',
        style: {
          color: Highcharts.getOptions().colors[1]
        }
      },
      type: 'line',
      name: 'Ph',
      tooltip: {
        formatter: function() {
          return 'Tarih: ' + Highcharts.dateFormat('%e %b %y %H:%M:%S', this.x) +
            'Ph: ' + this.y.toFixed(2);
        }
      }
    },
    {
    title: {
      text: 'TDS Values',
      style: {
        color: Highcharts.getOptions().colors[5]
      }
    },
    labels: {
      format: '{value}',
      style: {
        color: Highcharts.getOptions().colors[2]
      }
    },
      type: 'line',
      name: 'TDS Values',
      opposite: true,
      tooltip: {
        formatter: function() {
          return 'Tarih: ' + Highcharts.dateFormat('%e %b %y %H:%M:%S', this.x) +
            'TDS: ' + this.y.toFixed(2);
        }
      }
    }],
    series: [
      {
        id: 'phSeries',
        name: 'Ph',
        text: 'Ph Values',
        type: 'spline',
        turboThreshold: 7,
        data: [],
        color: '#FFEE11'
      },
      {
        name: 'TDS',
        text: 'TDS Values',
        type: 'spline',
        yAxis: 1,
        turboThreshold: 1000,
        data: []
      }
    ]
  };

  public phTDSAvarage: any =  {
    chart: {
        type: 'solidgauge',
        backgroundColor: 'transparent'
    },
    title: null,
    pane: {
        center: ['50%', '60%'],
        size: '100%',
        startAngle: -90,
        endAngle: 90,
        background: {
            backgroundColor:  '#EEE',
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc'
        },
        color: '#EEff12'
    },
    position: {
      align: 'left',
      verticalAlign: 'bottom',
      x: 10,
      y: -10
    },
    tooltip: {
        enabled: false
    },
    // the value axis
    yAxis: {
        stops: [
            [0.1, '#55BF3B'], // green
            [0.5, '#DDDF0D'], // yellow
            [0.9, '#DF5353'] // red
        ],
        lineWidth: 0,
        minorTickInterval: null,
        tickAmount: 2,
        title: {
            y: -70
        },
        labels: {
            y: 16
        }
    },
    plotOptions: {
        solidgauge: {
            dataLabels: {
                y: 5,
                borderWidth: 1,
                useHTML: true,
                color: '#FFEE11',
                pointFormat: '{point.y:.2f}'
            }
        }
    }
};


  subscription: Subscription;
  constructor(private hidroservice: HidromodService) { }

  ngOnInit() {
    // Set 10 seconds interval to update data again and again
    this.drawPhChart();
    this.drawPhandTdsMeter();
    setInterval(() => {
      this.drawPhChart();
      this.drawPhandTdsMeter();
    }, 10000);
  }

  drawPhChart() {
    let a = 0;
    this.subscription = this.hidroservice.getAllAuditLast100().subscribe(
      dataRes => {
        const seriePh = [];
        const serieTds = [];
        const givenph = [];
        dataRes.result.sort((d1, d2) => +new Date(d1.createTime) - +new Date(d2.createTime)).forEach(row => {
          console.log(' date : ' + JSON.stringify(row.createTime));
          const ph_row = [
            new Date(row.createTime).getTime(),
            row.ph
          ];
          const Tds_row = [
            new Date(row.createTime).getTime(),
            row.tds
          ];
          a++;
          if (a === 7) {
            const flag = {
              color: 'red',
              width: 2,
              value: new Date(row.createTime).getTime()
          };
            givenph.push(flag);
          }
          seriePh.push(ph_row);
          serieTds.push(Tds_row);
        });
        this.phListoptions.series[0]['data'] = seriePh;
        this.phListoptions.series[1]['data'] = serieTds;
        this.phListoptions['xAxis']['plotLines'] = givenph;
        Highcharts.chart('phList', this.phListoptions);
      },
      error => {
        console.log('Bişey ters gitti. ');
      });
  }

  drawPhandTdsMeter() {
    console.log(' Girdi :');
    this.subscription = this.hidroservice.getAvarageAudit('Ph').subscribe(data => {
       // The PH gauge
        const chartSpeed = Highcharts.chart('AvaragePgGeu', Highcharts.merge(this.phTDSAvarage, {
            yAxis: {
                min: 0,
                max: 12,
                title: {
                    text: 'PH METER',
                    color: '#2f7ed8',
                    y: 10
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'PH',
                data: [data.result] ? [data.result] : [0],
                tooltip: {
                    valueSuffix:  + ' Ph',
                    color: '#2f7ed8'
                },
                color: '#2f7ed8'
            }]
        }));
    });
    this.subscription = this.hidroservice.getAvarageAudit('Tds').subscribe(data => {
              // The TDS gauge
              const chartRpm = Highcharts.chart('AvarageTdsGeu', Highcharts.merge(this.phTDSAvarage, {
                yAxis: {
                    min: 0,
                    max: 1000,
                    title: {
                        text: 'TDS',
                        y: 10
                    }
                },
                series: [{
                    name: 'TDS',
                    data:  [data.result] ? [data.result] : [0],
                    tooltip: {
                        valueSuffix: ' revolutions/min'
                    }
                }]
            }));
    });

  }
}
