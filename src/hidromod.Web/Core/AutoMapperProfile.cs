﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using hidromod.Web.Data.Model;
using hidromod.Web.Requests.Model;
using hidromod.Web.ViewModels;

namespace hidromod.Web.Core
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Ayarlar, AyarlarDto>();
            CreateMap<Audit, AuditDto>();


            CreateMap<AuditPostDto, Audit>()
                .ForMember(m => m.Id, m => m.Ignore());
            CreateMap<AyarlarPostDto, Ayarlar>()
                .ForMember(m => m.Id, m => m.Ignore());

                
            CreateMap<ApplicationUser, UserViewModel>()
                   .ForMember(d => d.Roles, map => map.Ignore());
            CreateMap<UserViewModel, ApplicationUser>();

            CreateMap<ApplicationUser, UserEditViewModel>()
                .ForMember(d => d.Roles, map => map.Ignore());
            CreateMap<UserEditViewModel, ApplicationUser>();

            CreateMap<ApplicationUser, UserPatchViewModel>()
                .ReverseMap();

            CreateMap<IdentityRole, RoleViewModel>()
//                .ForMember(d => d.Permissions, map => map.MapFrom(s => s.Claims))
//                .ForMember(d => d.UsersCount, map => map.ResolveUsing(s => s.Users?.Count ?? 0))
                .ReverseMap();
            CreateMap<RoleViewModel, IdentityRole>();

            CreateMap<IdentityRoleClaim<string>, ClaimViewModel>()
                .ForMember(d => d.Type, map => map.MapFrom(s => s.ClaimType))
                .ForMember(d => d.Value, map => map.MapFrom(s => s.ClaimValue))
                .ReverseMap();

            CreateMap<ApplicationPermission, PermissionViewModel>()
                .ReverseMap();

            CreateMap<IdentityRoleClaim<string>, PermissionViewModel>()
                .ConvertUsing(s => Mapper.Map<PermissionViewModel>(ApplicationPermissions.GetPermissionByValue(s.ClaimValue)));
        }
    }
}
